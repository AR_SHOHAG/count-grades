#include<stdio.h>

int main()
{
    int n;
    printf("Number of students: ");
    scanf("%d", &n);


    int arr[n],i,j, check=0;
    printf("Enter obtained marks of the students: ");
    for(i=0;i<n; i++){
        scanf("%d",&arr[i]);
    }

    for(i=0;i<n; i++){
        if(arr[i]>=80 && arr[i]<=100)
        check++;
    }
    printf("Number of A+ = %d \n",check);

    check=0;

    for(i=0;i<n; i++){
        if(arr[i]>=75 && arr[i]<80)
        check++;
    }
    printf("Number of A  = %d \n",check);

    check=0;

    for(i=0;i<n; i++){
        if(arr[i]>=70 && arr[i]<75)
        check++;
    }
    printf("Number of A- = %d \n",check);

    check=0;

    for(i=0;i<n; i++){
        if(arr[i]>=65 && arr[i]<70)
        check++;
    }
    printf("Number of B+ = %d \n",check);

    check=0;

    for(i=0;i<n; i++){
        if(arr[i]>=60 && arr[i]<65)
        check++;
    }
    printf("Number of B  = %d \n",check);

    check=0;

    for(i=0;i<n; i++){
        if(arr[i]>=55 && arr[i]<60)
        check++;
    }
    printf("Number of B- = %d \n",check);

    check=0;

    for(i=0;i<n; i++){
        if(arr[i]>=50 && arr[i]<55)
        check++;
    }
    printf("Number of C+ = %d \n",check);

    check=0;

    for(i=0;i<n; i++){
        if(arr[i]>=45 && arr[i]<50)
        check++;
    }
    printf("Number of C  = %d \n",check);

    check=0;

    for(i=0;i<n; i++){
        if(arr[i]>=40 && arr[i]<45)
        check++;
    }
    printf("Number of D  = %d \n",check);

    check=0;

    for(i=0;i<n; i++){
        if(arr[i]<40)
        check++;
    }
    printf("Number of F  = %d \n",check);


}
